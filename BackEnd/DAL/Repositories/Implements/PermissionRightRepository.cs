﻿using DAL.Entities;
using DAL.Infrastructure;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Implements
{
    public class PermissionRightRepository : RepositoryBase<PermissionRight>, IPermissionRightRepository
    {
        public PermissionRightRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public async Task<bool> SetPermission(long id, int PermissionId, int RightId)
        {
            var exist = await _dbSet.Where(x => x.IdRole == id).FirstOrDefaultAsync();
            if (exist != null)
            {
                exist.IdRight = RightId;
                exist.IdPermission = PermissionId;
                _dbSet.Update(exist);

                //Save Changes by UnitOfWork Commit()
            }
            else
            {
                throw new Exception("No User with that id");
                return false;
            }
            return true;
        }
        
        #region Group 5 - Authentication & Authorization
        public IEnumerable<PermissionRight> GetPermissionsRightsByRoleId(long roleId)
        {
            return this._dbSet.Where(x => x.IdRole == roleId);
        }
        #endregion
    }
}
