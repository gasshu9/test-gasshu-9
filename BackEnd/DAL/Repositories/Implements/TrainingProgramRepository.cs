﻿using DAL.Entities;
using DAL.Infrastructure;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using Org.BouncyCastle.Asn1.X509;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Implements
{
  public class TrainingProgramRepository : RepositoryBase<TrainingProgram>, ITrainingProgramRepository
  {
    private readonly FRMDbContext _dbContext;
    private readonly IUnitOfWork _unitOfWork;

    public TrainingProgramRepository(IDbFactory dbFactory, IUnitOfWork unitOfWork) : base(dbFactory)
    {
      _dbContext = dbFactory.Init();
      _unitOfWork = unitOfWork;
    }
    public async Task<List<TrainingProgram>> GetAll()
    {
      var count = await _dbSet.CountAsync();
      return await _dbSet.Skip(count > 200 ? count - 200 : 0).Take(count)
          .Include(x => x.HistoryTrainingPrograms).ThenInclude(x => x.User)
          .Include(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.Sessions).ThenInclude(x => x.Units).ThenInclude(x => x.Lessons)
          .ToListAsync();
    }
    public async Task<bool> Delete(long id)
    {
      var exist = await _dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
      if (exist != null && exist.Status == 1)
      {

        _dbSet.Remove(exist);
        await _dbContext.SaveChangesAsync();

        return true;
      }
      else
      {
        return false;
      }
    }
    public async Task<List<TrainingProgram>> Edit(long id, string name)
    {

      var exist = await _dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
      if (exist != null)
      {
        exist.Name = name;


      }


      return await _dbSet.ToListAsync();
    }
    public async Task<bool> DeActivate(long id)
    {
      var program = await _dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
      if (program == null)
        return false;
      program.Status = 1;
      _dbSet.Update(program);
      _unitOfWork.commitAsync();
      return true;
    }

    public async Task<long> Duplicate(long id)
    {
      string c = "(Copy)";
      var oldtrainingPropgram = await _dbSet.Include(x => x.HistoryTrainingPrograms).ThenInclude(x => x.User)
                .Include(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.Sessions).ThenInclude(x => x.Units).ThenInclude(x => x.Lessons).FirstOrDefaultAsync(x => x.Id == id);
      if (oldtrainingPropgram == null)
        return 0;
      var newPropgram = new TrainingProgram();
      newPropgram.Name = oldtrainingPropgram.Name + "Copy";
      newPropgram.Status = oldtrainingPropgram.Status;
      if (oldtrainingPropgram.HistoryTrainingPrograms != null)
      {
        var ListHistoryTrainingProgram = new List<HistoryTrainingProgram>();
        foreach (var historyTrainingProgram in oldtrainingPropgram.HistoryTrainingPrograms)
        {
          HistoryTrainingProgram historyTrainingProgram1 = new HistoryTrainingProgram()
          {
            ModifiedOn = historyTrainingProgram.ModifiedOn,
            User = historyTrainingProgram.User,
          };
          ListHistoryTrainingProgram.Add(historyTrainingProgram1);
        }
        newPropgram.HistoryTrainingPrograms = ListHistoryTrainingProgram;
      }
      if (oldtrainingPropgram.Curricula != null)
      {
        var ListCurricula = new List<Curriculum>();
        foreach (var curricula in oldtrainingPropgram.Curricula)
        {
          Curriculum newcurricula = new Curriculum()
          {
            NumberOrder = curricula.NumberOrder,
            Syllabus = curricula.Syllabus

          }
          ;
          ListCurricula.Add(newcurricula);
        }
        newPropgram.Curricula = ListCurricula;
      }
      //Console.WriteLine(oldtrainingPropgram.Classes != null);
      //if (oldtrainingPropgram.Classes != null)
      //{
      //    var ListClasses = new List<Class>();
      //    foreach (var classes in oldtrainingPropgram.Classes)
      //    {
      //        Class classes1 = new Class()
      //        {
      //            Name = classes.Name,
      //            ClassCode = classes.ClassCode,
      //            Status = classes.Status,
      //            StartTimeLearning = classes.StartTimeLearning,
      //            EndTimeLearing = classes.EndTimeLearing,
      //            CreatedOn = DateTime.Now,
      //            PlannedAtendee = classes.PlannedAtendee,
      //            AcceptedAttendee = classes.AcceptedAttendee,
      //            ActualAttendee = classes.ActualAttendee,
      //            CurrentSession = classes.CurrentSession,
      //            CurrentUnit = classes.CurrentUnit,
      //            StartYear = classes.StartYear,
      //            StartDate = classes.StartDate,
      //            EndDate = classes.EndDate,
      //            ClassNumber = classes.ClassNumber,
      //            IdTechnicalGroup = classes.IdTechnicalGroup,
      //            IdAttendeeType = classes.IdAttendeeType,
      //            IdFormatType = classes.IdFormatType,
      //            IdFSU = classes.IdFSU,
      //            IdFSUContact= classes.IdFSUContact,
      //            IdSite = classes.IdSite,
      //            IdStatus =classes.IdStatus,
      //            IdProgramContent=classes.IdProgramContent,
      //            IdUniversity= classes.IdUniversity
      //        };
      //        ListClasses.Add(classes1);
      //    }
      //    newPropgram.Classes = ListClasses;
      //}

      _dbSet.Add(newPropgram);
      _unitOfWork.commitAsync();
      return oldtrainingPropgram.Id;
    }

    public async Task<List<TrainingProgram>> GetByFilter(List<string> programNames)
    {
      var result = new List<TrainingProgram>();
      foreach (var name in programNames)
      {
        var trainingPrograms = await _dbSet.Where(x => x.Name.Contains(name) || x.HistoryTrainingPrograms.First().User.UserName.Contains(name))
            .Include(x => x.HistoryTrainingPrograms).ThenInclude(x => x.User)
            .Include(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.Sessions).ThenInclude(x => x.Units).ThenInclude(x => x.Lessons)
            .ToListAsync();
        foreach (var trainingProgram in trainingPrograms)
        {
          if (!result.Contains(trainingProgram))
          {
            result.Add(trainingProgram);
          }
        }
      }
      return result;
    }

    public async Task<TrainingProgram> GetById(long id)
    {
      var result = await _dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
      if (result == null)
      {
        return new TrainingProgram();
      }
      else
      {
        return result;
      }
    }

    public async Task<bool> Edit(long id, string name, int status)
    {
      var program = await _dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
      if (program == null)
      {
        return false;
      }
      program.Name = name;
      program.Status = status;
      _dbSet.Update(program);
      _unitOfWork.commitAsync();
      return true;
    }
    public TrainingProgram Create(TrainingProgram trainingProgram)
    {
      _dbSet.Add(trainingProgram);

      _dbContext.SaveChanges();

      return trainingProgram;
    }

    public TrainingProgram GetbyId(long id)
    {
      return _dbSet.Include(x => x.HistoryTrainingPrograms)
                   .Include(x => x.Curricula)
                   .FirstOrDefault(x => x.Id == id);
    }

    //Training Program
    public List<TrainingProgram> GetTraingProgramAll()
    {
      return _dbSet.ToList();
    }

    public List<TrainingProgram> GetTraingProgramAllById(long programId)
    {
      return _dbSet.Where(x => x.Id == programId && x.Status != 3 && x.Status != 6).ToList();
    }


    public TrainingProgram CreateTrainingProgram(TrainingProgram trainingProgram)
    {
      _dbSet.Add(trainingProgram);
      _dbContext.SaveChanges();
      return trainingProgram;
    }

    //Training Program
    public async Task<List<TrainingProgram>> Search(string name)
    {
      var res = await _dbSet
          .Include(x => x.HistoryTrainingPrograms).ThenInclude(x => x.User)
          .Include(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.HistorySyllabi)
          .Include(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.Sessions).ThenInclude(x => x.Units)
          .ThenInclude(x => x.Lessons).ThenInclude(x => x.DeliveryType)
          .Include(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.Sessions).ThenInclude(x => x.Units)
          .ThenInclude(x => x.Lessons).ThenInclude(x => x.OutputStandard)
          .Include(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.Sessions).ThenInclude(x => x.Units)
          .ThenInclude(x => x.Lessons).ThenInclude(x => x.FormatType)
          .Where(x => (x.Name == default || x.Name.Contains(name)))
          .ToListAsync();

      return res;
    }
    //team4
    public List<TrainingProgram> GetAllForImport()
    {
      return this._dbSet.ToList();
    }

    public void AddForImport(string name, int status)
    {
      var training = new TrainingProgram { Name = name, Status = 1 };
      _dbContext.Add(training);
      _dbContext.SaveChanges();

    }


  }
}

