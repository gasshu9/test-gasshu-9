﻿using DAL.Entities;
using DAL.Infrastructure;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Implements
{
    public class HistorySyllabusRepository : RepositoryBase<HistorySyllabus>, IHistorySyllabusRepository
    {
        // Team6
        private readonly FRMDbContext _dbContext;
        public HistorySyllabusRepository(IDbFactory dbFactory) : base(dbFactory)
        {
            _dbContext = dbFactory.Init();
        }

        public void CreateHistorySyllabus(HistorySyllabus historySyllabus)
        {
            _dbSet.Add(historySyllabus);
        }

        public void AddHistorySyllabusForImport(HistorySyllabus historySyllabus)
        {
            _dbSet.Add(historySyllabus);
        }

        public IQueryable<HistorySyllabus> GetHistorySyllabus()
        {
            return _dbSet;
        }

        // Team6

        public List<HistorySyllabus> GetAllHistorySyllabus()
        {
            return this._dbSet.ToList();
        }

        
    }
}