﻿using DAL.Entities;
using DAL.Infrastructure;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories.Implements
{
  public class ClassRepository : RepositoryBase<Class>, IClassRepository
  {
    private readonly FRMDbContext _dbContext;
    private readonly IUnitOfWork _unitOfWork;

    public ClassRepository(IDbFactory dbFactory, IUnitOfWork unitOfWork) : base(dbFactory)
    {
      _dbContext = dbFactory.Init();
      _unitOfWork = unitOfWork;
    }
    public async Task<bool> DeleteClass(long id)
    {
      var exist = await _dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
      if (exist != null && exist.Status == 1)
      {

        _dbSet.Remove(exist);
        _unitOfWork.commitAsync();


        return true;
      }
      else
      {
        return true;
      }
    }


    public Class GetById(long Id)
    {
      return _dbSet.FirstOrDefault(x => x.Id == Id);
    }

    public async Task<bool> DeActivate(long id)
    {
      var classDetail = await _dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
      if (classDetail == null)
        return false;
      classDetail.Status = 1;
      _dbSet.Update(classDetail);
      _unitOfWork.commitAsync();
      return true;
    }

    public async Task<long> Duplicate(long Id)
    {
      string c = "(Copy)";
      var target = await _dbSet.Include(x => x.Locations)
          .Include(x => x.ClassAdmins)
          .Include(x => x.ClassMentors)
          .Include(x => x.ClassTrainees).FirstOrDefaultAsync(x => x.Id == Id);
      if (target == null)

        return 0;
      var newClass = new Class();

      newClass.Name = target.Name.Insert(target.Name.Length, c);
      newClass.ClassCode = target.ClassCode;
      newClass.Status = target.Status;
      newClass.StartTimeLearning = target.StartTimeLearning;
      newClass.EndTimeLearing = target.EndTimeLearing;
      newClass.ReviewedBy = target.ReviewedBy;
      newClass.ReviewedUser = target.ReviewedUser;
      newClass.ReviewedOn = target.ReviewedOn;
      newClass.CreatedBy = target.CreatedBy;
      newClass.CreatedUser = target.CreatedUser;
      newClass.CreatedOn = DateTime.Now;
      newClass.ApprovedBy = target.ApprovedBy;
      newClass.ApprovedUser = target.ApprovedUser;
      newClass.ApprovedOn = DateTime.Now;
      newClass.PlannedAtendee = target.PlannedAtendee;
      newClass.ActualAttendee = target.ActualAttendee;
      newClass.AcceptedAttendee = target.AcceptedAttendee;
      newClass.CurrentSession = target.CurrentSession;
      newClass.CurrentUnit = target.CurrentUnit;
      newClass.StartYear = DateTime.Now.Year;
      newClass.StartDate = DateTime.Now;
      newClass.EndDate = target.EndDate;
      newClass.ClassNumber = target.ClassNumber;
      newClass.IdProgram = target.IdProgram;
      newClass.TrainingProgram = target.TrainingProgram;
      newClass.IdTechnicalGroup = target.IdTechnicalGroup;
      newClass.classTechnicalGroup = target.classTechnicalGroup;
      newClass.IdFSU = target.IdFSU;
      newClass.FsoftUnit = target.FsoftUnit;
      newClass.IdFSUContact = target.IdFSUContact;
      newClass.FSUContactPoint = target.FSUContactPoint;
      newClass.IdStatus = target.IdStatus;
      newClass.ClassStatus = target.ClassStatus;
      newClass.IdSite = target.IdSite;
      newClass.ClassSite = target.ClassSite;
      newClass.IdUniversity = target.IdUniversity;
      newClass.IdFormatType = target.IdFormatType;
      newClass.IdProgramContent = target.IdProgramContent;
      newClass.ClassProgramCode = target.ClassProgramCode;
      newClass.IdAttendeeType = target.IdAttendeeType;
      newClass.AttendeeType = target.AttendeeType;
      if (target.Locations != null)
      {
        var listLocation = new List<ClassLocation>();
        foreach (var classlocation in target.Locations)
        {
          ClassLocation newlocation = new ClassLocation()
          {
            IdLocation = classlocation.IdLocation,
          };
          listLocation.Add(newlocation);
        }
        newClass.Locations = listLocation;
      }

      newClass.ClassSelectedDates = target.ClassSelectedDates;
      newClass.ClassUpdateHistories = target.ClassUpdateHistories;
      if (target.ClassTrainees != null)
      {
        var listtrainees = new List<ClassTrainee>();
        foreach (var classtrainees in target.ClassTrainees)
        {
          ClassTrainee newtrainees = new ClassTrainee()
          {
            IdUser = classtrainees.IdUser,
          };
          listtrainees.Add(newtrainees);
        };



        newClass.ClassTrainees = listtrainees;
      }
      if (target.ClassMentors != null)
      {
        var listMentors = new List<ClassMentor>();
        foreach (var classmentor in target.ClassMentors)
        {
          ClassMentor newclassmentor = new ClassMentor()
          {
            IdUser = classmentor.IdUser,
          };
          listMentors.Add(newclassmentor);
        };
        newClass.ClassMentors = listMentors;
      }
      if (target.ClassAdmins != null)
      {
        var listAdmins = new List<ClassAdmin>();
        foreach (var classAdmin in target.ClassAdmins)
        {
          ClassAdmin newclassAdmin = new ClassAdmin()
          {
            IdUser = classAdmin.IdUser,
          };
          listAdmins.Add(newclassAdmin);
        };



        newClass.ClassAdmins = listAdmins;
      }

      _dbSet.Add(newClass);
      _unitOfWork.commitAsync();

      return newClass.Id;



    }

    public async Task<Class> GetDetail(long id)
    {
      var result = await _dbSet.Where(x => x.Id == id)
          .Include(x => x.ApprovedUser)
          .Include(x => x.CreatedUser)
          .Include(x => x.FsoftUnit).ThenInclude(x => x.FSUContactPoints)
          .Include(x => x.ClassSelectedDates)
          .Include(x => x.ClassFormatType)
          .Include(x => x.Locations).ThenInclude(x => x.Location)
          .Include(x => x.ClassProgramCode)
          .Include(x => x.ReviewedUser)
          .Include(x => x.ClassSite)
          .Include(x => x.classTechnicalGroup)
          .Include(x => x.ClassTrainees).ThenInclude(x => x.User)
          .Include(x => x.ClassUniversityCode)
          .Include(x => x.ClassMentors).ThenInclude(x => x.User)
          .Include(x => x.TrainingProgram).ThenInclude(x => x.HistoryTrainingPrograms).ThenInclude(x => x.User)
          .Include(x => x.TrainingProgram).ThenInclude(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.Sessions).ThenInclude(x => x.Units).ThenInclude(x => x.Lessons)
          .FirstOrDefaultAsync();

      if (result == null)
        return new Class();

      return result;
    }

    public async Task<Class> GetAttende(long idClass)
    {
      var result = await _dbSet.Where(x => x.Id == idClass)
          .Include(x => x.ClassTrainees).ThenInclude(x => x.User).ThenInclude(x => x.Role)
          .Include(x => x.ClassAdmins).ThenInclude(x => x.User).ThenInclude(x => x.Role)
          .Include(x => x.ClassMentors).ThenInclude(x => x.User).ThenInclude(x => x.Role)
          .FirstOrDefaultAsync();

      if (result == null)
        return new Class();
      return result;
    }

    public async Task<Class> GetTrainingProgram(long id)
    {
      var result = await _dbSet.Where(x => x.Id == id)
          .Include(x => x.TrainingProgram).ThenInclude(x => x.HistoryTrainingPrograms).ThenInclude(x => x.User)
          .Include(x => x.TrainingProgram).ThenInclude(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.Sessions).ThenInclude(x => x.Units).ThenInclude(x => x.Lessons)
          .FirstOrDefaultAsync();

      if (result == null)
        return new Class();
      return result;
    }

    public async Task<Class> GetClassById(long idClass)
    {
      return await _dbContext.Classes.Include(x => x.Locations).ThenInclude(x => x.Location)
                               .Include(x => x.ClassAdmins).ThenInclude(x => x.User)
                               .Include(x => x.ClassMentors).ThenInclude(x => x.User).FirstOrDefaultAsync(x => x.Id == idClass);
    }

    public IEnumerable<Class> GetWithFilter(Class entity)
    {
      List<Class> classList = new();

      if (entity.Id != null)
      {
        classList = _dbContext.Classes.Where(x => x.Id == entity.Id).ToList();
      }
      else if (entity.ClassCode != null)
      {
        classList = _dbContext.Classes.Where(x => x.ClassCode.Equals(entity.ClassCode)).ToList();
      }
      else if (entity.StartDate != null)
      {
        classList = _dbContext.Classes.Where(x => x.StartDate == entity.StartDate).ToList();
      }
      else if (entity.EndDate != null)
      {
        classList = _dbContext.Classes.Where(x => x.EndDate == entity.EndDate).ToList();
      }
      else if (entity.StartTimeLearning != null && entity.EndTimeLearing != null)
      {
        classList = _dbContext.Classes.Where(x => x.StartTimeLearning == entity.StartTimeLearning && x.EndTimeLearing == entity.EndTimeLearing).ToList();
      }
      else if (entity.IdFSU != null)
      {
        classList = _dbContext.Classes.Where(x => x.IdFSU == entity.IdFSU).ToList();
      }
      else if (entity.IdStatus != null)
      {
        classList = _dbContext.Classes.Where(x => x.IdStatus == entity.IdStatus).ToList();
      }
      else if (entity.IdAttendeeType != null)
      {
        classList = _dbContext.Classes.Where(x => x.IdAttendeeType == entity.IdAttendeeType).ToList();
      }

      return classList;
    }



    public void UpdateClass(Class _class)
    {
      var checkId = _dbSet.FirstOrDefault(x => x.Id == _class.Id);
      if (checkId != null)
      {
        _dbContext.Entry<Class>(checkId).State = EntityState.Detached;
        _dbSet.Update(_class);
      }

    }


    public async Task<List<Class>> GetClassByClassCode(string classCode)
    {
      var a = await _dbSet
         .Include(x => x.ClassStatus)
         .Include(x => x.Locations).ThenInclude(x => x.Location)
         .Include(x => x.ReviewedUser)
         .Include(x => x.CreatedUser)
         .Include(x => x.ApprovedUser)
         .Include(x => x.classTechnicalGroup)
         .Include(x => x.ClassSite)
         .Include(x => x.ClassUniversityCode)
         .Include(x => x.ClassFormatType)
         .Include(x => x.ClassProgramCode)
         .Include(x => x.ClassSelectedDates)
         .Include(x => x.ClassTrainees).ThenInclude(x => x.User).ThenInclude(x => x.Role)
         .Include(x => x.ClassAdmins).ThenInclude(x => x.User)
         .Include(x => x.ClassMentors).ThenInclude(x => x.User)
         .Include(x => x.FsoftUnit).ThenInclude(x => x.FSUContactPoints)
         .Include(x => x.TrainingProgram).ThenInclude(x => x.HistoryTrainingPrograms).ThenInclude(x => x.User)
         .Include(x => x.TrainingProgram).ThenInclude(x => x.Curricula).ThenInclude(x => x.Syllabus)
         .ThenInclude(x => x.Sessions).ThenInclude(x => x.Units)
         .ThenInclude(x => x.Lessons).ThenInclude(x => x.DeliveryType)
         .Include(x => x.TrainingProgram).ThenInclude(x => x.Curricula).ThenInclude(x => x.Syllabus)
         .ThenInclude(x => x.Sessions).ThenInclude(x => x.Units)
         .ThenInclude(x => x.Lessons).ThenInclude(x => x.OutputStandard)
         .Include(x => x.TrainingProgram).ThenInclude(x => x.Curricula).ThenInclude(x => x.Syllabus)
         .ThenInclude(x => x.Sessions).ThenInclude(x => x.Units)
         .ThenInclude(x => x.Lessons).ThenInclude(x => x.FormatType)
         .Include(x => x.TrainingProgram).ThenInclude(x => x.Curricula).ThenInclude(x => x.Syllabus).ThenInclude(x => x.HistorySyllabi)
         .Where(x => (x.ClassCode == default || x.ClassCode.Contains(classCode)))
         .ToListAsync();
      return a;

    }




    #region bhhiep
    public IQueryable<Class> GetClassesQuery()
    {
      return _dbSet;
    }
    #endregion
    public void CreateClassForImport(Class _class)
    {
      _dbSet.Add(_class);
      _dbContext.SaveChanges();
      return;
    }
    public List<Class> GetClasses()
    {
      var list = _dbSet.ToList();
      return list;
    }


    public IEnumerable<Class> GetClassess(List<string> key, List<long> location, DateTime? classTimeFrom, DateTime? classTimeTo, List<long> classTime, List<long> status, List<long> attendee, int FSU, int trainer)
    {
      //filter
      var classes = _dbSet.AsQueryable();
      // Search by location
      if (location != null && location.Count > 0)
      {
        classes = classes.Where(c => c.Locations.Any(l => location.Contains(l.IdLocation)));
      }
      // Search by class time from
      if (classTimeFrom != null)
      {
        classes = classes.Where(c => c.StartDate >= classTimeFrom);
      }
      // Search by class time to
      if (classTimeTo != null)
      {
        classes = classes.Where(c => c.EndDate <= classTimeTo);
      }
      // TODO: Search by classTime 
      // Search by status
      if (status != null && status.Count > 0)
      {
        classes = classes.Where(c => status.Contains(c.Status));

      }
      // Search by attendee
      if (attendee != null && attendee.Count > 0)
      {
        classes = classes.Where(c => attendee.Contains(c.IdAttendeeType ?? 0));
      }
      // Search by FSU
      if (FSU != 0)
      {
        classes = classes.Where(c => c.IdFSU == FSU);
      }
      // Search by trainer
      if (trainer != 0)
      {
        classes = classes.Where(c => c.ClassTrainees.Any(t => t.IdUser == trainer));
      }
      // search
      if (key != null)
      {
        foreach (var item in key)
        {
          var list = classes.Where(s => s.Name.ToLower().Contains(item.ToLower()) || s.ClassCode.ToLower().Contains(item.ToLower()))
                           .Select(s => s);
          classes = list;
        }
      }

      return classes.ToList();
    }
  }
}
