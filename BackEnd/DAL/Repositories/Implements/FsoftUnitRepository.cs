﻿using DAL.Entities;
using DAL.Infrastructure;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Implements
{
    public class FsoftUnitRepository : RepositoryBase<FsoftUnit>, IFsoftUnitRepository
    {
        public FsoftUnitRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public List<FsoftUnit> GetAll()
        {
            return this._dbSet.ToList();
        }

        public  async Task<List<FsoftUnit>> GetFSUs()
        {
            return await this._dbSet.ToListAsync();
        }
    }
}
