﻿using DAL.Entities;
using DAL.Infrastructure;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Implements
{
    public class ClassUniversityCodeRepository : RepositoryBase<ClassUniversityCode>, IClassUniversityCodeRepository
    {
        public ClassUniversityCodeRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public List<ClassUniversityCode> GetAll()
        {
            return this._dbSet.ToList();
        }
    }
}
