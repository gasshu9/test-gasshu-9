using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace FRMAPI.Helpers
{
  public class TokenHelpers
  {
    public static JwtSecurityToken ReadToken(HttpContext httpContext)
    {
      string token = httpContext.Session.GetString("TOKEN");
      var handler = new JwtSecurityTokenHandler();
      var jsonToken = handler.ReadToken(token) as JwtSecurityToken;
      return jsonToken;
    }
  }
}