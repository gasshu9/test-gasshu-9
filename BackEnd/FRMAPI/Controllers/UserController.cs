﻿using BAL.Models;
using BAL.Services.Interfaces;
using DAL.Entities;
using DAL.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using System.Reflection;
using DAL.Repositories.Interfaces;
using BAL.Services.Implements;
using System.Numerics;
using static DAL.Entities.User;

namespace FRMAPI.Controllers;
[Route("api/[controller]/[action]")]
[ApiController]
public class UserController : Controller
{
    User initUser = new User();
    List<User> detailUser = new List<User>();
    private readonly IUserService _userService;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IRoleService _roleService;
    private readonly IUserRepository _userRepository;
    private readonly IRoleRepository _roleRepository;
    private readonly IPermissionRightService _permissionRightService;
    private readonly IPermissionRepository _permissionRepository;
    public UserController(IUserService userService, IUnitOfWork unitOfWork, IRoleService roleService, IPermissionRightService permissionRightService)
    {
        _userService = userService;
        _unitOfWork = unitOfWork;
        _roleService = roleService;
        _permissionRightService = permissionRightService;

    }

    #region GetUser
    /// <summary>
    /// 
    /// </summary>
    /// <param name="keywords">search to name</param>
    /// <param name="sortBy">SortID:ID, SortFullName:FullName, SortEmail:Email, SortDateOfBirth:DateOfBirth, SortGender:Gender, SortStatus:Status</param>
    /// <param name="PAGE_SIZE">size of user</param>
    /// <param name="page">page</param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(typeof(List<UserViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetUserAsync([FromQuery] string? keywords, [FromQuery] string? sortBy, int PAGE_SIZE, int page=1)
    {
        
        var result = _userService.GetAll(keywords, sortBy,PAGE_SIZE, page);
        return new JsonResult(new
        {
            result
        });
    

    }
    #endregion

    #region AddUser
    /// <summary>
    /// UC10-002
    /// Add a new user 
    /// </summary>
    /// <remarks>
    ///     Sample request:
    ///
    ///         POST 
    ///         {
    ///             "userName": "Hoa",
    ///             "password": "123123",
    ///             "fullname": "Ngoc Hoa",
    ///             "dateOfBirth": "2022-10-31T08:30:36.640Z",
    ///             "gender": "M",
    ///             "phone": "2316843218",
    ///             "email": "hoa@gmail.com",
    ///             "address": "HCM",
    ///             "status": 1,
    ///             "idRole": 2
    ///         }
    ///               
    /// </remarks>
    /// <returns>A new user in the system</returns>
    /// <response code="200">Return user list</response>
    /// <response code="404">If the list is null</response>
    [HttpPost]
    [ProducesResponseType(typeof(List<UserViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> AddNewUserAsync([FromBody] UserAccountViewModel user)
    {
        string errorMessage = "";
        bool status = true;

        var result = new UserViewModel();
        try
        {
            result = await _userService.Add(user);
            if (result == null)
            {
                errorMessage = "Username or Email are exist";
                status = false;
            }
        }
        catch (Exception ex)
        {

            errorMessage = ex.Message;
        }


        return Ok(new
        {
            status = status,
            result = result,
            ErrorMessage = errorMessage
        });
    }
    #endregion

    #region ChangleUserRole
    /// <summary>
    /// UC10-005
    /// Change the user's role
    /// </summary>
    /// <param name="id"></param>
    /// <param name="IdRole"></param>
    /// 
    /// <remarks>
    ///     Sample request:
    ///
    ///         POST 
    ///         {
    ///             "userId": 2,
    ///             "roleId": 1
    ///         }
    ///         
    /// </remarks>
    /// <returns>A user whose role has been changed</returns>
    /// <response code="200">Return users </response>
    /// <response code="400">If the list is null</response>
    [HttpPut("{id}")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> ChangeUserRoleAsync(long id, long IdRole)
    {
        string errorMessage = "";
        bool status = false;
        var result = new UserViewModel();
        try
        {
            result = await _userService.ChangleRole(id, IdRole);
            status = true;
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
        }
        return Ok(new
        {
            status = status,
            result = result,
            ErrorMessage = errorMessage
        });
    }
    #endregion

    #region EditUser
    /// <summary>
    /// UC10-006
    /// Edit user's information
    /// </summary>
    /// <param name="id"></param>
    /// <param name="Fullname">Enter Fullname</param>
    /// <param name="Email">Enter Email</param>
    /// <param name="Dob">Enter Date of birth</param>
    /// <param name="Gender">Choose gender</param>
    /// <param name="Status">Choose status</param>
    /// <param name="IdRole"></param>   
    /// 
    /// <remarks>
    ///     Sample request:
    ///
    ///         POST 
    ///         "ID":1
    ///         "Fullname" : Trong Tan
    ///         "Dob" :12-10-2001
    ///         "Gender":M
    ///         "Email":Tan@gmail.com
    ///         "Status":1
    ///         "IdRole":1
    ///         
    /// </remarks>
    /// <returns>An edited user in the system</returns>
    /// <response code="200">Return users </response>
    /// <response code="400">If the list is null</response>
    [HttpPut("{id}")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> EditUserAsync(long id, string Fullname, DateTime Dob, char Gender, string Email, int Status, long IdRole)
    {
        string errorMessage = "";
        bool status = true;
        var result = new UserViewModel();
        try
        {
            result = await _userService.Edit(id, Fullname, Dob, Gender, Email, Status, IdRole);
            if (result == null)
            {
                errorMessage = "Email are exist";
                status = false;
            }
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
        }
        return Ok(new
        {
            status = status,
            result = result,
            ErrorMessage = errorMessage
        });
    }
    #endregion

    #region ImportUser
    /// <summary>
    /// UC10-007
    /// Import a user from a template file to the list
    /// </summary>
    /// <param name="FileName">this is file xls/xlxs to import user</param>
    /// 
    /// <remarks>
    ///     Sample request:
    ///
    ///         GET 
    ///         
    ///         "File(xlsx,xls)": ["xxx.xls"]
    ///         "Import template": download

    ///
    /// </remarks>
    /// <returns>return user mangement</returns>
    /// <response code="200">return User list page</response>
    /// <response code="400">If the list is null</response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> ImportUser([FromForm] UpLoadExcelFileRequest request)
    {
        UpLoadExcelFileResponse response = new UpLoadExcelFileResponse();
        string Path = "UploadFileUser/" + request.File.FileName;
        try
        {

            using (FileStream stream = new FileStream(Path, FileMode.CreateNew))
            {
                await request.File.CopyToAsync(stream);
            }


            response = await _userService.ImportUser(request, Path);

        }
        catch (Exception ex)
        {
            response.IsSuccess = false;
            response.Message = ex.Message;

        }
        return Ok(response);
    }

    #endregion

    #region DeactivateUser
    /// <summary>
    /// UC10-008
    /// Deactivate selected user, set user status to inactive
    /// </summary>
    /// <param name="id"></param>
    /// <remarks>
    ///     Sample request:
    ///
    ///         GET 
    ///         "Id" : 1,
    ///         "Status": 0
    ///          
    /// </remarks>
    /// <returns>Return result of action and error message (if any)</returns>
    /// <response code="200">Successful message</response>
    /// <response code="404">User's account is null</response>
    [HttpPut("[action]/{id}")]
    [ProducesResponseType(typeof(List<UserViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeactivateUserAsync(long id)
    {
        string errorMessage = "";
        bool status = false;

        try
        {
            status = await _userService.DeActivate(id);
        }
        catch (Exception ex)
        {
            status = false;
            errorMessage = ex.Message;
        }

        //End coding session

        return new JsonResult(new
        {
            status = status
        });
    }
    #endregion

    #region DeleteUser
    /// <summary>
    /// UC10-009
    /// Delete selected user
    /// </summary>
    /// <param name="id"></param>
    /// <remarks>
    ///     Sample request:
    ///
    ///         GET 
    ///         "Id" : 4
    ///          
    /// </remarks>
    /// <returns>Return result of action and error message (if any)</returns>
    /// <response code="200">Successful message</response>
    /// <response code="404">User's account is null</response>
    [HttpDelete("{id}")]
    [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteUserAsync(long id)
    {
        string errorMessage = "";
        bool status = false;
        try
        {
            status = await _userService.Delete(id);
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
        }
        return new JsonResult(new
        {
            status = status
        });
    }
    #endregion 

    #region GetRole
    /// <summary>
    /// UC10-010
    /// Shows list of roles
    /// </summary>
    /// <param name="role"></param>
    /// <remarks>
    /// </remarks>
    /// <returns>A list of roles in the system</returns>
    /// <response code="200">A list of roles in the system</response>
    /// <response code="404">List of roles is null</response>
    [HttpGet]
    [ProducesResponseType(typeof(List<RoleViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetRoleAsync()
    {
        string errorMessage = "";
        bool status = false;
        var result = new List<RoleViewModel>();
        try
        {
            result = await _roleService.GetAllRole();
            status = true;
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
        }
        return new JsonResult(new
        {
            UserList = result,
            status = status,
            errorMessage = errorMessage
        });
    }
    #endregion 

    #region AddRole
    /// <summary>
    /// UC10-011
    /// Add new role
    /// </summary>
    /// <param name="name"></param>
    /// <remarks>
    ///     Sample request:
    ///
    ///         POST 
    ///         {
    ///             "Name" : "Fresher"
    ///         }
    ///              
    /// </remarks>
    /// <returns>A list of roles</returns>
    /// <response code="200">Return role list</response>
    /// <response code="404">If the list is null</response>
    [HttpPost]
    [ProducesResponseType(typeof(RoleViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> AddNewRoleAsync(string name)
    {
        string errorMessage = "";
        bool status = false;
        Role result = new Role();
        try
        {
            result = await _roleService.AddNewRoleAsync(name);
            status = true;
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
        }
        return Ok(new
        {
            status = status,
            ErrorMessage = errorMessage,
            result = result
        });
    }
    #endregion

    #region SetRolePermission
    /// <summary>
    /// UC10-012
    /// Set permission to the role
    /// </summary>
    /// <param name="roleId"></param>
    /// <param name="Syllabus"></param>
    /// <param name="TrainingProgram"></param>
    /// <param name="Class"></param>
    /// <param name="Learning material"></param>
    /// <remarks>
    ///     Sample request:
    ///
    ///         POST 
    ///         "roleId": 1
    ///         "Syllabus": Access denied
    ///         "Training Program": View
    ///         "Class": Modify
    ///         "Learning Material": Create
    ///         "User": 
    ///         
    /// </remarks>
    /// <returns>A list of roles</returns>
    /// <response code="200">Return role list</response>
    /// <response code="404">If the list is null</response>
    [HttpPut]
    [ProducesResponseType(typeof(List<RoleViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> SetRolePermissionAsync(long roleId, int PermissionId, int RightId)
    {
        string errorMessage = "";
        bool status = false;
        var result = new RoleViewModel();



        try
        {
            //_userService.DeActivateUser(id);
            //_userService.Save();
            status = await _permissionRightService.SetPermission(roleId, RightId, PermissionId); ;
        }
        catch (Exception ex)
        {
            status = false;
            errorMessage = ex.Message;
        }

        return Ok(new
        {
            status = status,
            result = result,
            errorMessage = errorMessage
        });
    }
    #endregion
}

