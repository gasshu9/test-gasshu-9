﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Docker.DotNet;
using Docker.DotNet.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace xUnitTest
{
    public static class DockerUltils
    {
        public const string DB_PASSWORD = "MyStr0ngP@ssworD";
        public const string DB_USER = "SA";
        public const string DB_IMAGE = "mcr.microsoft.com/mssql/server";
        public const string DB_IMAGE_TAG = "2019-latest";
        public const string DB_CONTAINER_NAME = "FRMAPI";
        public const string VOLUME_NAME = "FRMAPI_Docker_Volume";

        public static async Task<(string containerID, string port)> GetContainerAndPort()
        {

            await CleanUpRunningContainers();
            await CleanUpRunningVolumes();

            var dockerClient = GetDockerClient();
            var freePort = GetFreePort();

            //create Image
            await dockerClient.Images.CreateImageAsync(new ImagesCreateParameters { 
                FromImage = $"{DB_IMAGE}:{DB_IMAGE_TAG}"
            }, null, new Progress<JSONMessage>());

            //create Volume
            var volumeList = await dockerClient.Volumes.ListAsync();
            var volumeCount = volumeList.Volumes.Where(v => v.Name == VOLUME_NAME).Count();
            if(volumeCount <= 0)
            {
                await dockerClient.Volumes.CreateAsync(new VolumesCreateParameters {
                    Name = VOLUME_NAME,
                });
            }

            //create Container
            var containers = await dockerClient.Containers.ListContainersAsync(new ContainersListParameters()
            {
                All = true
            });
            //create Container - get all existing containers related to test DB
            var existingContainer = containers.Where(c => c.Names.Any(n => n.Contains(DB_CONTAINER_NAME))).FirstOrDefault();
            //create new
            if(existingContainer == null)
            {
                var sqlContainer = await dockerClient.Containers.CreateContainerAsync(new CreateContainerParameters { Name = DB_CONTAINER_NAME,
                    Image = $"{DB_IMAGE}:{DB_IMAGE_TAG}",
                    Env = new List<string> {
                        "ACCEPT_EULA=Y",
                        $"SA_PASSWORD={DB_PASSWORD}"
                    },
                    HostConfig= new HostConfig
                    {
                        PortBindings = new Dictionary<string, IList<PortBinding>>
                        {
                            {
                                "1433/tcp",
                                new PortBinding[]
                                {
                                    new PortBinding
                                    {
                                        HostPort = freePort.ToString()
                                    }
                                }
                            }
                        },
                        Binds= new List<string>
                        {
                            $"{VOLUME_NAME}:/Accessioning_data"
                        }
                    }
                    });
                await dockerClient.Containers.StartContainerAsync(sqlContainer.ID, new ContainerStartParameters());

                var currents = await dockerClient.Containers.ListContainersAsync(new ContainersListParameters { All = true });
                var current = currents.Where(c => c.Names.Any(n => n.Contains(DB_CONTAINER_NAME))).FirstOrDefault();

                await WaitForDatabase(int.Parse(current.Ports.FirstOrDefault().PublicPort.ToString()));
                return (sqlContainer.ID, current.Ports.FirstOrDefault().PublicPort.ToString());
            }
            await WaitForDatabase(int.Parse(existingContainer.Ports.FirstOrDefault().PublicPort.ToString()));
            return (existingContainer.ID, existingContainer.Ports.FirstOrDefault().PublicPort.ToString());
            
        }
        public static async Task DisposeAfterUse()
        {
            await CleanUpContainersAfterUse();
            await CleanUpVolumesAfterUse();
        }
        private static async Task WaitForDatabase(int Port)
        {
            var start = DateTime.UtcNow;
            const int MAXWAITINSEC = 60;
            var connectionEstablished = false;

            while(!connectionEstablished && start.AddSeconds(MAXWAITINSEC) >= DateTime.UtcNow)
            {
                try
                {
                    var sqlConnetionString = GetSqlConnectionString(Port);
                    using var sqlConnection = new SqlConnection(sqlConnetionString);
                    await sqlConnection.OpenAsync();
                    connectionEstablished = true;

                    return;
                }
                catch(Exception ex) {
                    await Task.Delay(500);
                }
            }

            if (!connectionEstablished)
            {
                throw new Exception("Connection can not be established. Connection time out <!>");
            }
        }
        public static string GetSqlConnectionString(int port)
        {
            //try to get the machin's IP later
            return $"Data source=localhost,{port};" 
                //+ "Intergrated Sercurity=False;"
                + $"User ID={DB_USER};"
                + $"Password={DB_PASSWORD}";
        }
        private static int GetFreePort()
        {
            var tcpListener = new TcpListener(IPAddress.Loopback, 0);
            tcpListener.Start();
            var port = ((IPEndPoint) tcpListener.LocalEndpoint).Port;
            tcpListener.Stop();
            return port;
        }
        private static bool IsRunningOnWindows() {
            return Environment.OSVersion.Platform == PlatformID.Win32NT;
        }
        private static DockerClient GetDockerClient()
        {
            var dockerUri = IsRunningOnWindows()
                            ? "npipe://./pipe/docker_engine"
                            : "unix:///var/rung/docker.sock";
            return new DockerClientConfiguration(new Uri(dockerUri)).CreateClient();
        }
        private async static Task CleanUpRunningVolumes(int Expiration = -24)
        {
            var dockerClient = GetDockerClient();
            var runningVolumes = await dockerClient.Volumes.ListAsync();

            foreach(var runningVolume in runningVolumes.Volumes.Where(v => v.Name == VOLUME_NAME))
            {
                var expiration = Expiration < 0 ? Expiration * -1 : Expiration;
                if(DateTime.Parse(runningVolume.CreatedAt) < DateTime.UtcNow.AddHours(expiration)){
                    try
                    {
                        await StopAndRemoveVolumeAsync(runningVolume.Name);
                    }catch(Exception ex) { 
                    
                    }
                }
            }
        }
        private static async Task StopAndRemoveVolumeAsync(string name)
        {
            var dockerClient = GetDockerClient();
            await dockerClient.Volumes.RemoveAsync(name);
        }
        private async static Task CleanUpRunningContainers(int Expiration = -24)
        {
            var dockerCLient = GetDockerClient();
            var runningContainers = await dockerCLient.Containers.ListContainersAsync(new ContainersListParameters());

            foreach (var runningContainer in runningContainers.Where(container => container.Names.Any(x => x.Contains(DB_CONTAINER_NAME))))
            {
                //Stop all containers that exceed 24 hours lifetime
                var expiration = Expiration > 0 ? Expiration * -1 : Expiration;

                if(runningContainer.Created < DateTime.UtcNow.AddHours(expiration))
                {
                    try
                    {
                        await StopAndRemoveContainersAsync(runningContainer.ID);
                    }catch(Exception ex)
                    {
                        
                    }
                }

            }
        }
        private static async Task StopAndRemoveContainersAsync(string ID)
        {
            var dockerClient = GetDockerClient();
            await dockerClient.Containers.StopContainerAsync(ID, new ContainerStopParameters());
            await dockerClient.Containers.RemoveContainerAsync(ID, new ContainerRemoveParameters());
        }
        private async static Task CleanUpContainersAfterUse()
        {
            var dockerCLient = GetDockerClient();
            var runningContainers = await dockerCLient.Containers.ListContainersAsync(new ContainersListParameters());

            foreach (var runningContainer in runningContainers.Where(container => container.Names.Any(x => x.Contains(DB_CONTAINER_NAME))))
            {

                
                    try
                    {
                        await StopAndRemoveContainersAsync(runningContainer.ID);
                    }
                    catch (Exception ex)
                    {

                    }


            }
        }
        private async static Task CleanUpVolumesAfterUse()
        {
            var dockerClient = GetDockerClient();
            var runningVolumes = await dockerClient.Volumes.ListAsync();

            foreach (var runningVolume in runningVolumes.Volumes.Where(v => v.Name == VOLUME_NAME))
            {

                    try
                    {
                        await StopAndRemoveVolumeAsync(runningVolume.Name);
                    }
                    catch (Exception ex)
                    {

                    }

            }
        }

    }
}
