﻿using BAL.Models;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DAL.Entities.User;

namespace BAL.Services.Interfaces
{
    public interface IUserService
    {
        Task<User> FindByOtp(string Otp);
        Task<User> FindByEmail(string email);
        Task<AccountViewModel> Login(string email, string password);
        Task<List<UserViewModel>> GetAllUser();
        Task<UserViewModel> Add(UserAccountViewModel user);
        Task<UserViewModel> Edit(long id, string Fullname, DateTime Dob, char Gender, string Email, int Status, long IdRole);
        public Task<UpLoadExcelFileResponse> ImportUser(UpLoadExcelFileRequest request, string path);
        Task<bool> DeActivate(long id);
        Task<UserViewModel> ChangleRole(long id, long IdRole);
        Task<bool> Delete(long id);
        List<UserViewModel> GetAll(string? keyword, string? sortBy, int PAGE_SIZE,int page=1);

        #region Other groups
        User GetByID(long id);
        Task<IEnumerable<TrainerViewModel>> GetTrainers();
        #region Group 5 - Authentication & Authorization
        public User GetUser(string username);
        public User GetUser(string username, string password);
        #endregion
        UserViewModel GetUserViewModelById(long userId);
        #endregion

        #region Functions
        void Save();
        void SaveAsync();
        #endregion
    }
}
