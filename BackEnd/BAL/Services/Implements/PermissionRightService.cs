﻿using AutoMapper;
using BAL.Models;
using BAL.Services.Interfaces;
using DAL.Entities;
using DAL.Infrastructure;
using DAL.Repositories.Implements;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Services.Implements
{
    public class PermissionRightService: IPermissionRightService
    {
        private IPermissionRightRepository _permissionRightRepository;
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public PermissionRightService(IPermissionRightRepository permissionRightRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _permissionRightRepository = permissionRightRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        #region Group 5 - Authentication & Authorization
        public IEnumerable<PermissionRight> GetPermissionRightsByRoleId(long roleId)
        {
            return _permissionRightRepository.GetPermissionsRightsByRoleId(roleId);
        }
        #endregion
        public async Task<bool> SetPermission(long id, int PermissionId, int RightId)
        {

            bool result1 = await _permissionRightRepository.SetPermission(id, PermissionId, RightId);
           
            //var result = _mapper.Map<List<UserViewModel>>(await _userRepository.DeActivate(id));
            SaveAsync();
            return result1;

        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void SaveAsync()
        {
            _unitOfWork.commitAsync();
        }
    }
}
